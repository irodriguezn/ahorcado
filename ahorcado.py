""" Juego del Ahorcado """


import entradaSalida as es
import cadenas as ca

def compruebaLetra(palabraG, palabra, letra):
    lista=ca.devuelvePosiciones(palabra, letra)
    if lista==[]: return -1
    cadena=ca.rellenaLetras(palabraG,lista,letra)
    return cadena

def main():
    intentos=fallos=0
    letras=letra=""
    palabraAdivinar=ca.quitaTildes(es.seleccionaPalabra())
    longitud=len(palabraAdivinar)
    palabraGuiones = "-" * longitud
    pantalla=ca.intercalar(palabraGuiones,1)
    print(pantalla)
    while True:
        letra=input("Introduce letra o resuelve: ")
        if len(letra)>1:
            if letra==palabraAdivinar:
                print("ché, quin destral, has guanyat!")
                print("T'ha costat", intentos+1, "intents")
                break
            else:
                fallos+=1
                es.dibujaFallos(fallos)
                if fallos>=6:
                    print("ché, quin borinot, has perdut!")
                    break
            intentos+=1
            pantalla=ca.intercalar(palabraGuiones,1)
            print("Letras probadas:",letras)
            print(pantalla)            
            continue
        if letra in letras:
            print("paquete, esa ya la has dicho!!")
            continue
        letras+=letra
        intentos+=1
        res = compruebaLetra(palabraGuiones, palabraAdivinar, letra)
        if res != -1: # acertamos letra
            palabraGuiones=res
            if palabraGuiones==palabraAdivinar:
                print("ché, quin destral, has guanyat!")
                print("T'ha costat", intentos+1, "intents")
                break
        else:
            fallos+=1
            es.dibujaFallos(fallos)
            if fallos>=6:
                print("ché, quin borinot, has perdut!")
                break
        pantalla=ca.intercalar(palabraGuiones,1)
        print("Letras probadas:",letras)
        print(pantalla)

    
if __name__ == "__main__":
    main()
