""" Programa que prueba la función
    randrange para comprobar si
    proporciona números aleatorios
    de manera proporcionada"""

from random import randrange

def tiraDado():
    return randrange(6)+1

def realizaTiradas(numTiradas):
    """ Inicializamos las variables
        que contarán cada una de las
        caras del dado que van saliendo """

    c1=c2=c3=c4=c5=c6=0
    for i in range(numTiradas):
        valorDado=tiraDado()
        if valorDado==1:
            c1+=1
        elif valorDado==2:
            c2+=1
        elif valorDado==3:
            c3+=1
        elif valorDado==4:
            c4+=1
        elif valorDado==5:
            c5+=1
        elif valorDado==6:
            c6+=1
    return (c1, c2, c3, c4, c5, c6)

def main():
    c1,c2,c3,c4,c5,c6=realizaTiradas(1000)
    print("1-", c1)
    print("2-", c2)    
    print("3-", c3)
    print("4-", c4)
    print("5-", c5)
    print("6-", c6)
    
main()
