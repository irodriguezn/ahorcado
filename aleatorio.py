from random import random

# Función que devuelve un entero del 1 al num
def aleatorio(num):
    return int(random()*num+1)

# Función que devuelve un entero entre n y m
def aleaEntre(n, m):
    return int(random()*(m-n+1)+n)

# Función que devuelve un entero del 1 al 6
def tiraDado()
    return aleatorio(6)

def main():
    for i in range(50):
        print(aleaEntre(5,10), " ", end="")

main()
