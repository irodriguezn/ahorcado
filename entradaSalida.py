from random import randint

def seleccionaPalabra():
    listaPalabras=[]
    for linea in open('palabras.txt'):
        listaPalabras.append(linea)
    pos=randint(0, len(listaPalabras)-1)
    palabraElegida=listaPalabras[pos]
    return palabraElegida.strip()

def dibujaGuiones(num):
    print("- " * num)

def dibujaFallos(num):
    if num==1:
        print("   ____    ")
        print("  |   |    ")
        print("  |   O    ")
        print("  |        ")
        print("  |        ")
        print("  |        ")
        print("  --       ")
        print("  ---      ")
    elif num==2:
        print("   ____    ")
        print("  |   |    ")
        print("  | \ O    ")
        print("  |        ")
        print("  |        ")
        print("  |        ")
        print("  --       ")
        print("  ---      ")
    elif num==3:
        print("   ____    ")
        print("  |   |    ")
        print("  | \ O /  ")
        print("  |        ")
        print("  |        ")
        print("  |        ")
        print("  --       ")
        print("  ---      ")
    elif num==4:
        print("   ____    ")
        print("  |   |    ")
        print("  | \ O /  ")
        print("  |   |    ")
        print("  |        ")
        print("  |        ")
        print("  --       ")
        print("  ---      ")
    elif num==5:
        print("   ____    ")
        print("  |   |    ")
        print("  | \ O /  ")
        print("  |   |    ")
        print("  |  /     ")
        print("  |        ")
        print("  --       ")
        print("  ---      ")
    elif num==6:
        print("   ____    ")
        print("  |   |    ")
        print("  | \ O /  ")
        print("  |   |    ")
        print("  |  / \   ")
        print("  |        ")
        print("  --       ")
        print("  ---      ")

def main(): # Función principal para probar el modulo
    print(dibujaGuiones(5))


if __name__ == "__main__":
    main()
