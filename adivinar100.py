from random import random

# Función que devuelve un número entero del 1 al num
def aleatorio(num):
    return int(random()*num+1)

def devuelveInsulto():
    insultos=["borinot", "palurdo", "calamidad", "político", "programadorcillo", "caraculo"]
    pos=aleatorio(len(insultos)-1)
    return insultos[pos]

def devuelveDespedida():
    insultos=["cara culo", "tontolaba", "cerebrín", "sobradete"]
    pos=aleatorio(len(insultos)-1)
    return insultos[pos]

def devuelvePiropo():
    piropos=["maquinón", "puto amo", "jugón", "jugadorazo", "profesorazo", "programadorazo", "programadón"]
    pos=aleatorio(len(piropos)-1)
    return piropos[pos]

def main():
    limiteIntentos=7
    salir=False
    while not salir:
        numBuscado=aleatorio(100)
        intentos=0
        finPartida=False
        print()
        print("Adivina un número del 1 al 100")
        print("------------------------------")
        while not finPartida:
            intentos+=1
            num=int(input("Introduce un número: "))
            if num==numBuscado:
                print("¡¡¡Acertaste, ", devuelvePiropo(), ", eres un hacha!!!")
                finPartida=True
            elif numBuscado>num:
                print("El número es mayor, ", devuelveInsulto(), "!!")
            elif numBuscado<num:
                print("El número es menor, ", devuelveInsulto(), "!!")
            if intentos>limiteIntentos:
                print("¡¡Perdiste, ", devuelveInsulto(), "!!")
                finPartida=True
            elif intentos<=limiteIntentos:
                if not finPartida:
                    print("El número de intentos hasta ahora es ", intentos)
                else:
                    print("El número de intentos final es ", intentos)
                print()
        op=input("¿Deseas jugar de nuevo (S/N? ")
        if op.upper()=="N":
            print()
            print("bye, ", devuelveDespedida(), "!!")
            salir=True

main()
