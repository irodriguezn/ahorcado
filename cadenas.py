# Módulo de funciones de cadena

def devuelvePosiciones(cadena, letra):
    listaPosiciones=[]
    pos=cadena.find(letra)
    while pos!=-1:
        listaPosiciones.append(pos)
        pos=cadena.find(letra, pos+1)
    return listaPosiciones

def intercalar(cadena, n):
    aux=""
    for car in cadena:
        aux+=car + " " * n
    return aux.strip()

def quitaTilde(vocal):
    v=vocal.lower()
    if v in ['á','à','ä','â']:
        return 'a'
    elif v in ['é','è','ë','ê']:
        return 'e'
    elif v in ['í','ì','ï','î']:
        return 'i'
    elif v in ['ó','ò','ö','ô']:
        return 'o'
    elif v in ['ú','ù','ü','û']:
        return 'u'
    else:
        return v
    
def quitaTildes(cadena):
    aux=""
    for car in cadena:
        aux+=quitaTilde(car)
    return aux

def rellenaLetras(cadenaGuiones,posiciones, letra):
    lista=list(cadenaGuiones)
    for pos in posiciones:
        lista[pos]=letra
    cadena="".join(lista)
    return cadena

def main():
    palabra="árábànätâ"
    car="a"
    print(palabra," ", car)
    lista=devuelvePosiciones(quitaTildes(palabra),car)
    print(lista)

if __name__ == "__main__":
    main()
